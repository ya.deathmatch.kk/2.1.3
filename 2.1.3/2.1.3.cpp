﻿// 2.1.3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <istream>

using namespace std;

class Vector // создаем класс вектор
{
public:
    Vector(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;

    }
    friend Vector operator+ (const Vector& a, const Vector& b);  //создаем дружественную функцию и после и перенпужаем оператор, в данном случае -
    friend Vector operator- (const Vector& a, const Vector& b);
    friend Vector operator* (const Vector& a, const Vector& b);
    
    friend ostream &operator<<(ostream& out, const Vector& v); // дружественная функция оператора вывода


    friend istream& operator>>(istream& in,  Vector& m);// аналогично создаем дружественную функцию для оператора ввода,  и перегружаем ее
private:
    float x;
    float y;
    float z;



};



Vector operator+(const Vector& a, const Vector& b) // имеет доступ к приватным члеам класса Vector
{
    return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vector operator-(const Vector& a, const Vector& b) // имеет доступ к приватным члеам класса Vector
{
    return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

Vector operator*(const Vector& a, const Vector& b) // имеет доступ к приватным члеам класса Vector
{
    return Vector(a.x * b.x, a.y * b.y, a.z * b.z);
}



ostream& operator<<(ostream& out, const Vector& v)
{
    out<< " " << v.x <<" "<< v.y <<" " <<v.z;
    return out;
}



istream& operator>>(istream& in,  Vector& m) // перегружаем опреатор ввода
{
   in >> m.x;
   in >> m.y;
   in >> m.z;
    return in;
}


int main()
{
    Vector v1(0, 1, 2);
    Vector v2(3, 4, 5);
    cout << v1 + v2 << endl;
    cout << v1 * v2 << endl;
    cout << v1 - v2 << endl;
   // cout << x - y << endl;
   // cout << x*y << endl;
   // cout << v1 - v2 << endl;
    

    cout << "Enter x,y,z coordinates for Vector v1:\n";
    cin >> v2;  // если не создать дружественную функцию опреатора ввода и не перегрузить ее то ввод не будет раьотать
    cout << "Enter x,y,z coordinates for Vector v2:\n";
    cin >> v1; // и вывод будет тех значений, которые были введены изначально
    cout << v1 + v2 << endl;
    cout << v1 * v2 << endl;
    cout << v1 - v2 << endl;
}

